@extends('layouts.app')
@section('content')
    <div class="container justify-content-center">
        <div class="row">
            <div class="col col-md-6 offset-md-3">
                <div class="card mt-5">
                    <div class="card-header">
                        <h2 class="text-center">CREATE TASK</h2>
                    </div>

                    <div class="card-body">
                        <form action="{{route('tasks.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input class="form-control mb-2" name="name" placeholder="Name ...">
                                @error('name')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <textarea class="form-control mb-2" name="content" rows="4" placeholder="Content ..."></textarea>
                                @error('content')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success" type="submit" value="SUBMIT">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
